# Membuat Soal Pilihan Ganda (PG) dengan PHP

## Things todo list:
1. Clone this repository di folder htdocs anda: `git clone https://gitlab.com/php-native1/soal-pg-sample1.git`
2. Masuk ke folder `cd soal-pg-sample1`
3. Jalankan `db_quest.sql` file d PhpMyAdmin anda:
4. Bukan Browser favorite anda. Kemudian ketikkan http://localhost/soal-pg-sample1

## Source
Repositori diambil dari link berikut ini: https://www.yellowweb.id/blog/membuat-soal-pilihan-ganda-dengan-php/